
<?php include 'include/header.php';?>
        <div class="container">
            <nav class="d-flex align-items-center breadcrumb">
                <a href="">หน้าหลัก</a>
                <a class="active" href="">ข่าวสาร</a>
            </nav>
        </div>
        <div class="container">
            <section class="banner banner-news d-flex align-items-center">
                <h1>ข่าวสาร</h1>
            </section>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-12 col-md-3">
                    <h2 class="title-list">
                        โครงการของเรา
                    </h2>
                    <ul class="main-list">
                        <li>
                            <a href="">สถานีไฟฟ้าแรงสูง</a>
                        </li>
                        <li>
                            <a href="">โรงไฟฟ้า</a>
                        </li>
                        <li>
                            <a href="">พลังงานทางเลือก</a>
                        </li>
                        <li>
                            <a href="">โรงงานปิโตรเคมี โรงกลั่น</a>
                        </li>
                        <li>
                            <a href="">เหมืองแร่</a>
                        </li>
                        <li>
                            <a href="">งานก่อสร้างโรงงานและธนาคาร</a>
                        </li>
                        <li>
                            <a href="">สถานีบริการน้ำมัน</a>
                        </li>
                    </ul>
                </div>
                <div class="col-12 col-md-9">
                    <h2 class="title-blue">
                        สถานีไฟฟ้าแรงสูง
                    </h2>
                    <div class="container-fluid">
                        <div class="row no-gutters wrap-news">
                            <div class="col-12 col-md-4">
                                <a href="" class="news">
                                    <figure>
                                        <img src="https://via.placeholder.com/350x250 " alt="">
                                    </figure>
                                    <div class="news_body">
                                        <h5 class="title">
                                            title
                                        </h5>
                                        <p class="des">
                                            Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                        </p>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <p class="date">
                                                20/05/2561
                                            </p>
                                            <p class="btn">
                                                อ่านต่อ
                                            </p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-md-4">
                                <a href="" class="news">
                                    <figure>
                                        <img src="https://via.placeholder.com/350x250 " alt="">
                                    </figure>
                                    <div class="news_body">
                                        <h5 class="title">
                                            title
                                        </h5>
                                        <p class="des">
                                            Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                        </p>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <p class="date">
                                                20/05/2561
                                            </p>
                                            <p class="btn">
                                                อ่านต่อ
                                            </p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-md-4">
                                <a href="" class="news">
                                    <figure>
                                        <img src="https://via.placeholder.com/350x250 " alt="">
                                    </figure>
                                    <div class="news_body">
                                        <h5 class="title">
                                            title
                                        </h5>
                                        <p class="des">
                                            Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                        </p>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <p class="date">
                                                20/05/2561
                                            </p>
                                            <p class="btn">
                                                อ่านต่อ
                                            </p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-md-4">
                                <a href="" class="news">
                                    <figure>
                                        <img src="https://via.placeholder.com/350x250 " alt="">
                                    </figure>
                                    <div class="news_body">
                                        <h5 class="title">
                                            title
                                        </h5>
                                        <p class="des">
                                            Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                        </p>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <p class="date">
                                                20/05/2561
                                            </p>
                                            <p class="btn">
                                                อ่านต่อ
                                            </p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-md-4">
                                <a href="" class="news">
                                    <figure>
                                        <img src="https://via.placeholder.com/350x250 " alt="">
                                    </figure>
                                    <div class="news_body">
                                        <h5 class="title">
                                            title
                                        </h5>
                                        <p class="des">
                                            Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                        </p>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <p class="date">
                                                20/05/2561
                                            </p>
                                            <p class="btn">
                                                อ่านต่อ
                                            </p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-md-4">
                                <a href="" class="news">
                                    <figure>
                                        <img src="https://via.placeholder.com/350x250 " alt="">
                                    </figure>
                                    <div class="news_body">
                                        <h5 class="title">
                                            title
                                        </h5>
                                        <p class="des">
                                            Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                        </p>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <p class="date">
                                                20/05/2561
                                            </p>
                                            <p class="btn">
                                                อ่านต่อ
                                            </p>
                                        </div>
                                    </div>
                                </a>
                            </div>


                        </div>
                    </div>

                    <div class="d-flex justify-content-end">
                        <nav class="pagination">
                            <a href=""><i class="fal fa-angle-double-left"></i></a>
                            <a class="active" href="">1</a>
                            <a href="">2</a>
                            <a href="">3</a>
                            <a href=""><i class="fal fa-angle-double-right"></i></a>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

<?php include 'include/footer.php';?>


