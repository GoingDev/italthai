<?php include 'include/header.php';?>


        <div class="container">
            <nav class="d-flex align-items-center breadcrumb">
                <a href="">หน้าหลัก</a>
                <a class="active" href="">โครงการของเรา</a>
            </nav>
        </div>
        <div class="container">
            <section class="banner banner-ourproject d-flex align-items-center">
                <h1>โครงการของเรา</h1>
            </section>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-12 col-md-3">
                    <h2 class="title-list">
                        โครงการของเรา
                    </h2>
                    <ul class="main-list">
                        <li>
                            <a href="">สถานีไฟฟ้าแรงสูง</a>
                        </li>
                        <li>
                            <a href="">โรงไฟฟ้า</a>
                        </li>
                        <li>
                            <a href="">พลังงานทางเลือก</a>
                        </li>
                        <li>
                            <a href="">โรงงานปิโตรเคมี โรงกลั่น</a>
                        </li>
                        <li>
                            <a href="">เหมืองแร่</a>
                        </li>
                        <li>
                            <a href="">งานก่อสร้างโรงงานและธนาคาร</a>
                        </li>
                        <li>
                            <a href="">สถานีบริการน้ำมัน</a>
                        </li>
                    </ul>
                </div>
                <div class="col-12 col-md-9">
                    <div class="project-detail">
                        <h2 class="title-blue">
                            สถานีไฟฟ้าแรงสูง
                        </h2>
                        <div class="des">
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. In deserunt vero officia rem ea, magnam eos exercitationem tenetur odio corrupti, dicta enim! Rem minus suscipit cupiditate error obcaecati, facere enim.
                        </div>
                        <h5 class="mini-title">
                            mini-title
                        </h5>
                        <div class="grid-project-gallery">
                            <a data-fancybox="gallery" class="grid-project-gallery_item" href="https://via.placeholder.com/150"><img src="https://via.placeholder.com/150"></a>
                            <a data-fancybox="gallery" class="grid-project-gallery_item" href="https://via.placeholder.com/150"><img src="https://via.placeholder.com/300"></a>
                            <a data-fancybox="gallery" class="grid-project-gallery_item" href="https://via.placeholder.com/150"><img src="https://via.placeholder.com/100"></a>
                            <a data-fancybox="gallery" class="grid-project-gallery_item" href="https://via.placeholder.com/150"><img src="https://via.placeholder.com/250"></a>
                            <a data-fancybox="gallery" class="grid-project-gallery_item" href="https://via.placeholder.com/150"><img src="https://via.placeholder.com/450"></a>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>

<?php include 'include/footer.php';?>