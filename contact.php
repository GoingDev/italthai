<?php include 'include/header.php';?>



    <div class="g-map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3875.5350489294933!2d100.57155201532517!3d13.746576090350878!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e29ef7cc94d4d7%3A0x92216cebd91706f7!2sItalthai+Tower+(ITD)!5e0!3m2!1sen!2sth!4v1542609501320"
            width="600" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
    <div class="container-fluid bg-contact-form">
        <div class="container p-0">
            <div class="row no-gutters">
                <div class="col-12 col-md-6">
                    <div>
                        <h5 class="title-list-contact">
                            บริษัท อาคารอิตัลไทยวิศวกรรม จำกัด
                        </h5>
                        <ul class="list-contact">
                            <li>
                                <i class="far fa-map-marker-alt"></i>
                                <p>
                                    2034/124 อาคารอิตัลไทยทาวเวอร์ ชัน 29 <br>
                                    ถนนเพชรบุรีตัดใหม่ แขวงบางกะปิ <br>
                                    เขตห้วยขวาง กรุงเทพ 10310
                                </p>
                            </li>
                            <li>
                                <i class="fas fa-phone"></i>
                                <div class="d-flex flex-column">
                                    <a class="linkTel" href="">
                                        <p>โทร.</p>
                                        <p>0-2723-4420-5</p>
                                    </a>
                                    <a href="">
                                        <p>แฟกซ์</p>
                                        <p>0-2723-4427</p>
                                    </a>
                                </div>
                            </li>
                            <li>
                                <i class="fal fa-envelope"></i>
                                <a href="">
                                    <p>อีเมล์:</p>
                                    <p>marketing@italthaiengineering.com</p>
                                </a>
                            </li>
                        </ul>
                        <h5 class="title-form">
                            ติดต่อเรา โปรดกรอกฟอร์มด้านล่าง
                        </h5>
                        <form class="form-contact" action="">
                            <div class="d-flex flex-wrap flex-lg-nowrap wrap-input">
                                <input type="text" placeholder="ชื่อ - นามสกุล">
                                <input type="email" placeholder="อีเมล">
                            </div>
                            <div class="wrap-input">
                                <textarea name="" id="" rows="5" placeholder="ข้อความ"></textarea>
                            </div>
                            <button class="btn-submit" type="submit">ส่งข้อมูล</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid wrap-box-contact">
        <div class="container">
            <div class="row no-gutters row-box-contact">
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="box-contact">
                        <h5 class="title">
                            ฝ่ายผู้เชี่ยวชาญระบบไฟฟ้า
                        </h5>
                        <h4 class="name">
                            นายธงชัย วิจารณ์ เจริญ
                        </h4>
                        <a href="">
                            <p>อีเมล</p>
                            <p>tcv@italthaiengineering.com</p>
                        </a>
                        <a href="">
                            <p>โทร.</p>
                            <p>0-2723-4420-5 ต่อ 4001</p>
                        </a>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="box-contact">
                        <h5 class="title">
                            ฝ่ายผู้เชี่ยวชาญระบบไฟฟ้า
                        </h5>
                        <h4 class="name">
                            นายธงชัย วิจารณ์ เจริญ
                        </h4>
                        <a href="">
                            <p>อีเมล</p>
                            <p>tcv@italthaiengineering.com</p>
                        </a>
                        <a href="">
                            <p>โทร.</p>
                            <p>0-2723-4420-5 ต่อ 4001</p>
                        </a>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="box-contact">
                        <h5 class="title">
                            ฝ่ายผู้เชี่ยวชาญระบบไฟฟ้า
                        </h5>
                        <h4 class="name">
                            นายธงชัย วิจารณ์ เจริญ
                        </h4>
                        <a href="">
                            <p>อีเมล</p>
                            <p>tcv@italthaiengineering.com</p>
                        </a>
                        <a href="">
                            <p>โทร.</p>
                            <p>0-2723-4420-5 ต่อ 4001</p>
                        </a>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="box-contact">
                        <h5 class="title">
                            ฝ่ายผู้เชี่ยวชาญระบบไฟฟ้า
                        </h5>
                        <h4 class="name">
                            นายธงชัย วิจารณ์ เจริญ
                        </h4>
                        <a href="">
                            <p>อีเมล</p>
                            <p>tcv@italthaiengineering.com</p>
                        </a>
                        <a href="">
                            <p>โทร.</p>
                            <p>0-2723-4420-5 ต่อ 4001</p>
                        </a>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="box-contact">
                        <h5 class="title">
                            ฝ่ายผู้เชี่ยวชาญระบบไฟฟ้า
                        </h5>
                        <h4 class="name">
                            นายธงชัย วิจารณ์ เจริญ
                        </h4>
                        <a href="">
                            <p>อีเมล</p>
                            <p>tcv@italthaiengineering.com</p>
                        </a>
                        <a href="">
                            <p>โทร.</p>
                            <p>0-2723-4420-5 ต่อ 4001</p>
                        </a>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="box-contact">
                        <h5 class="title">
                            ฝ่ายผู้เชี่ยวชาญระบบไฟฟ้า
                        </h5>
                        <h4 class="name">
                            นายธงชัย วิจารณ์ เจริญ
                        </h4>
                        <a href="">
                            <p>อีเมล</p>
                            <p>tcv@italthaiengineering.com</p>
                        </a>
                        <a href="">
                            <p>โทร.</p>
                            <p>0-2723-4420-5 ต่อ 4001</p>
                        </a>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="box-contact">
                        <h5 class="title">
                            ฝ่ายผู้เชี่ยวชาญระบบไฟฟ้า
                        </h5>
                        <h4 class="name">
                            นายธงชัย วิจารณ์ เจริญ
                        </h4>
                        <a href="">
                            <p>อีเมล</p>
                            <p>tcv@italthaiengineering.com</p>
                        </a>
                        <a href="">
                            <p>โทร.</p>
                            <p>0-2723-4420-5 ต่อ 4001</p>
                        </a>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="box-contact">
                        <h5 class="title">
                            ฝ่ายผู้เชี่ยวชาญระบบไฟฟ้า
                        </h5>
                        <h4 class="name">
                            นายธงชัย วิจารณ์ เจริญ
                        </h4>
                        <a href="">
                            <p>อีเมล</p>
                            <p>tcv@italthaiengineering.com</p>
                        </a>
                        <a href="">
                            <p>โทร.</p>
                            <p>0-2723-4420-5 ต่อ 4001</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php include 'include/footer.php';?>