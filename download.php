<?php include 'include/header.php';?>
        <div class="container">
            <nav class="d-flex align-items-center breadcrumb">
                <a href="">หน้าหลัก</a>
                <a class="active" href="">ดาวน์โหลด</a>
            </nav>
        </div>
        <div class="container">
            <section class="banner banner-information d-flex align-items-center">
                <h1>ดาวน์โหลด</h1>
            </section>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-12 col-md-3">
                    <h2 class="title-list">
                        ดาวน์โหลด
                    </h2>
                    <ul class="main-list">
                        <li>
                            <a href="">วีดีทัศน์</a>
                        </li>
                        <li>
                            <a href="">ไฟล์</a>
                        </li>
                        <li>
                            <a href="">Italthai 60th Year Projection Mapping</a>
                        </li>
                    </ul>
                </div>
                <div class="col-12 col-md-9">
                    <div>
                        <h2 class="title-blue">
                            วีดีทัศน์
                        </h2>
                        <div class="video-y">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/rxyDz4mEGR0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                        <p>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, facilis veritatis recusandae, rerum excepturi earum quod adipisci, esse cupiditate soluta alias officia magnam laboriosam. Consequatur perspiciatis odit pariatur magnam perferendis?
                        </p>
                        <p>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Distinctio aperiam earum omnis nemo enim tempore, unde dolorem, quasi ratione facere repellendus aut atque harum quod explicabo reiciendis repudiandae pariatur veniam!
                        </p>
                        <a class="btn-blue btn-download btn-m50" href="">
                            ดาวน์โหลดข้อมูลบริษัท
                        </a>
                    </div>
                </div>
            </div>
        </div>

<?php include 'include/footer.php';?>











