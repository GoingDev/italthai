<?php include 'include/header.php';?>


        <div class="container">
            <nav class="d-flex align-items-center breadcrumb">
                <a href="">หน้าหลัก</a>
                <a class="active" href="">ดาวน์โหลด</a>
            </nav>
        </div>
        <div class="container">
            <section class="banner banner-jobUs d-flex align-items-center">
                <h1>ข่าวสารและกิจกรรม</h1>
            </section>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-12 col-md-3">
                    <h2 class="title-list">
                        ข่าวสารอื่นๆ
                    </h2>
                    <div>
                        
                        <form action="" class="form-find-job">
                            <h5 class="text-center title">ค้นหางาน</h5>
                            <select>
                                <option value="" selected disabled hidden>ตำแหน่งงาน</option>
                                <option value="ตำแหน่งงาน1">ตำแหน่งงาน1</option>
                                <option value="ตำแหน่งงาน2">ตำแหน่งงาน2</option>
                                <option value="ตำแหน่งงาน3">ตำแหน่งงาน3</option>
                                <option value="ตำแหน่งงาน4">ตำแหน่งงาน4</option>
                            </select>
                            <select>
                                <option value="" selected disabled hidden>ตำแหน่งงาน</option>
                                <option value="ตำแหน่งงาน1">ตำแหน่งงาน1</option>
                                <option value="ตำแหน่งงาน2">ตำแหน่งงาน2</option>
                                <option value="ตำแหน่งงาน3">ตำแหน่งงาน3</option>
                                <option value="ตำแหน่งงาน4">ตำแหน่งงาน4</option>
                            </select>
                            <select>
                                <option value="" selected disabled hidden>ตำแหน่งงาน</option>
                                <option value="ตำแหน่งงาน1">ตำแหน่งงาน1</option>
                                <option value="ตำแหน่งงาน2">ตำแหน่งงาน2</option>
                                <option value="ตำแหน่งงาน3">ตำแหน่งงาน3</option>
                                <option value="ตำแหน่งงาน4">ตำแหน่งงาน4</option>
                            </select>
                            <button type="submit">
                                ค้นหา
                            </button>
                        </form>
                    </div>
                </div>
                <div class="col-12 col-md-9">
                    <div>
                        <aside class="row no-gutters align-items-lg-center wrap-contact-job">
                            <div class="col-12 col-lg-7">
                                <div class="contact-job">
                                    <h6 class="mini-title">
                                        ผู้สมัครที่สนใจ สามารถส่งประวัติส่วนตัวและรูปถ่ายปัจจุบัน มาที่:
                                    </h6>
                                    <h5 class="title">
                                        ฝ่ายทรัพยากรมนุษย์
                                    </h5>
                                    <div>
                                        <p>บริษัท อิตัลไทยวิศกรรม จำกัด</p>
                                        <a href="">
                                            2034/124 อาคารอิตัลไทยทาวเวอร์ ชั้น 29 ถนนเพชรบุรีตัดใหม่ แขวงบางกะปิ
                                            เขตห้วยขวาง กรุงเทพฯ 10310
                                        </a>
                                        <a href="">
                                            โทร. 0-2723-4420-5 ต่อ 3115
                                        </a>
                                        <a href="">
                                            อีเมล์ hataichanok@Italthaiengineering.com
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-lg-4 offset-lg-1">
                                <div class="contact-job_list">
                                    <a href="">ค่าตอบแทนและสวัสดิการ</a>
                                    <a href="">การเดินทาง</a>
                                    <a href="">กิจกรรม</a>
                                </div>
                            </div>


                        </aside>
                        <div class="wrap-post-job">
                            <a class="post-job" href="">
                                <div class="wrap-title">
                                    <h6 class="mini-title">วิศวกรประเมินราคา</h6>
                                    <h2 class="title">
                                        วิศวกรโยธา - 2034/124วิศวกรโยธา - 2034/124วิศวกรโยธา - 2034/124วิศวกรโยธา -
                                        2034/124วิศวกรโยธา - 2034/124วิศวกรโยธา - 2034/124
                                    </h2>
                                </div>
                                <p class="position">
                                    รับสมัคร : หลายอัตรา
                                </p>
                                <p class="type">
                                    พนักงานประจำ
                                </p>
                                <p class="btn">
                                    รายละเอียด
                                </p>
                            </a>
                            <a class="post-job" href="">
                                <div class="wrap-title">
                                    <h6 class="mini-title">วิศวกรประเมินราคา</h6>
                                    <h2 class="title">
                                        วิศวกรโยธา - 2034/124วิศวกรโยธา - 2034/124วิศวกรโยธา - 2034/124วิศวกรโยธา -
                                        2034/124วิศวกรโยธา - 2034/124วิศวกรโยธา - 2034/124
                                    </h2>
                                </div>
                                <p class="position">
                                    รับสมัคร : หลายอัตรา
                                </p>
                                <p class="type">
                                    พนักงานประจำ
                                </p>
                                <p class="btn">
                                    รายละเอียด
                                </p>
                            </a>
                            <a class="post-job" href="">
                                <div class="wrap-title">
                                    <h6 class="mini-title">วิศวกรประเมินราคา</h6>
                                    <h2 class="title">
                                        วิศวกรโยธา - 2034/124วิศวกรโยธา 
                                    </h2>
                                </div>
                                <p class="position">
                                    รับสมัคร : หลายอัตรา
                                </p>
                                <p class="type">
                                    พนักงานประจำ
                                </p>
                                <p class="btn">
                                    รายละเอียด
                                </p>
                            </a>
                            <a class="post-job" href="">
                                <div class="wrap-title">
                                    <h6 class="mini-title">วิศวกรประเมินราคา</h6>
                                    <h2 class="title">
                                        วิศวกรโยธา - 2034/124วิศวกรโยธา
                                    </h2>
                                </div>
                                <p class="position">
                                    รับสมัคร : หลายอัตรา
                                </p>
                                <p class="type">
                                    พนักงานประจำ
                                </p>
                                <p class="btn">
                                    รายละเอียด
                                </p>
                            </a>
                        </div>

                    </div>
                </div>
            </div>
        </div>

<?php include 'include/footer.php';?>