<?php include 'include/header.php';?>



<div class="container">
    <nav class="d-flex align-items-center breadcrumb">
        <a href="">หน้าหลัก</a>
        <a class="active" href="">ดาวน์โหลด</a>
    </nav>
</div>


<div class="container">
    <section class="banner banner-about d-flex align-items-center">
        <h1>คณะกรรมการบริษัท</h1>
    </section>
</div>


<div class="container">
    <div class="row">
        <div class="col-12 col-md-3">
            <h2 class="title-list">
                เกี่ยวกับเรา
            </h2>
            <ul class="main-list">
                <li>
                    <a href="">
                        ประวัติ
                    </a>
                </li>
                <li>
                    <a href="">
                        วิสัย
                    </a>
                </li>
                <li>
                    <a href="">
                        ปรัชญา
                    </a>
                </li>
                <li>
                    <a href="">
                        วัฒ
                    </a>
                </li>
                <li>
                    <a href="">
                        คณะ
                    </a>
                </li>
            </ul>
        </div>
        <div class="col-12 col-md-9">

            <div>
                <h2 class="title-blue">
                    คณะกรรมการบริษัท
                </h2>
                <div class="container-fluid p-0">
                    <div class="row no-gutters">
                        <div class="col-12">
                            <figure class="card-person-one">
                                <div class="wrap-text">
                                    <h3 class="title">นายสกล เหล่าสุวรรณ</h3>
                                    <h5 class="des">ประธานเจ้าหน้าที่บริหาร</h5>
                                </div>
                                <img src="dist/img/human/board/สกลเหล่าสุวรรณ.png" alt="">
                            </figure>
                        </div>
                    </div>
                </div>
                <div class="container-fluid p-0">
                <div class="row no-gutters row-card-person">
                        <div class="col-12 col-md-4">
                            <figure class="card-person">
                                <img src="dist/img/human/board/ยุทธชัยจรณะจิตต์.png" alt="">
                                <figcaption>
                                    <h3 class="title">
                                        ยุทธชัย จรณะจิตต์
                                    </h3>
                                    <h5 class="des">
                                        ประธานเจ้าหน้าที่บริหาร
                                    </h5>
                                </figcaption>
                            </figure>
                        </div>
                        <div class="col-12 col-md-4">
                            <figure class="card-person">
                                <img src="dist/img/human/board/วลัยทิพย์พิริยะวรสกุล.png" alt="">
                                <figcaption>
                                    <h3 class="title">
                                        วลัยทิพย์ พิริยะวรสกุล
                                    </h3>
                                    <h5 class="des">
                                        กรรมการบริษัท
                                    </h5>
                                </figcaption>
                            </figure>
                        </div>
                        <div class="col-12 col-md-4">
                            <figure class="card-person">
                                <img src="dist/img/human/board/สกลเหล่าสุวรรณ.png" alt="">
                                <figcaption>
                                    <h3 class="title">
                                        สกล เหล่าสุวรรณ
                                    </h3>
                                    <h5 class="des">
                                        กกรมการบริษัท
                                    </h5>
                                </figcaption>
                            </figure>
                        </div>
                        <div class="col-12 col-md-4">
                            <figure class="card-person">
                                <img src="dist/img/human/board/นายณัษฐาประโมจนีย์.png" alt="">
                                <figcaption>
                                    <h3 class="title">
                                        นายณัษฐา ประโมจนีย์
                                    </h3>
                                    <h5 class="des">
                                        กรรมการบริษัท
                                    </h5>
                                </figcaption>
                            </figure>
                        </div>
                        <div class="col-12 col-md-4">
                            <figure class="card-person">
                                <img src="dist/img/human/board/สงครามทองนพคุณ.png" alt="">
                                <figcaption>
                                    <h3 class="title">
                                        สงคราม ทองนพคุณ
                                    </h3>
                                    <h5 class="des">
                                        กรรมการบริษัท
                                    </h5>
                                </figcaption>
                            </figure>
                        </div>
                        <div class="col-12 col-md-4">
                            <figure class="card-person">
                                <img src="dist/img/human/board/กอปรภีระวงศ์.png" alt="">
                                <figcaption>
                                    <h3 class="title">
                                        กอปร ภีรวงศ์
                                    </h3>
                                    <h5 class="des">
                                        กรรมการบริษัท
                                    </h5>
                                </figcaption>
                            </figure>
                        </div>
                        <div class="col-12 col-md-4">
                            <figure class="card-person">
                                <img src="dist/img/human/board/สมบูรณ์วงศ์รัศมี.png" alt="">
                                <figcaption>
                                    <h3 class="title">
                                        สมบูรณ์ วงศ์รัศมี
                                    </h3>
                                    <h5 class="des">
                                        กรรมการบริษัท
                                    </h5>
                                </figcaption>
                            </figure>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>








<?php include 'include/footer.php';?>