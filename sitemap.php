<?php include 'include/header.php';?>



        <div class="container">
            <nav class="d-flex align-items-center breadcrumb">
                <a href="">หน้าหลัก</a>
                <a class="active" href="">โครงการของเรา</a>
            </nav>
        </div>

        <div class="container">
            <div class="row row-box-sitemap">
                <div class="col-12 col-md-3">
                    <div class="box-sitemap">
                        <h5 class="title">
                            เกี่ยวกับเรา
                        </h5>
                        <ul class="box-sitemap_list">
                            <li>
                                <a href="">ประวัติความเป็นมา</a>
                            </li>
                            <li>
                                <a href="">ปรัชญา</a>
                            </li>
                            <li>
                                <a href="">ค่านิยมหลัก</a>
                            </li>
                            <li>
                                <a href="">คณะกรรมการบริษัทฯ</a>
                            </li>
                            <li>
                                <a href="">คณะผู้บริหาร</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-12 col-md-3">
                    <div class="box-sitemap">
                        <h5 class="title">
                            ระบบบริหาร
                        </h5>
                        <ul class="box-sitemap_list">
                            <li>
                                <a href="">สาส์นจากประธาน</a>
                            </li>
                            <li>
                                <a href="">ระบบบริหารคุณภาพ (QMS)</a>
                            </li>
                            <li>
                                <a href="">ระบบจัดการด้านความปลอดภัย</a>
                            </li>
                            <li>
                                <a href="">ระบบวางแผนทรัพยากรองค์กร (ERP)</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-12 col-md-3">
                    <div class="box-sitemap">
                        <h5 class="title">
                            ธรุกิจของเรา
                        </h5>
                        <ul class="box-sitemap_list">
                            <li>
                                <a href="">ฝ่ายผู้เชี่ยวชาญระบบไฟฟ้า</a>
                            </li>
                            <li>
                                <a href="">ฝ่ายธรุกิจพลังงาน</a>
                            </li>
                            <li>
                                <a href="">ฝ่ายโครงการ</a>
                            </li>
                            <li>
                                <a href="">ฝ่ายงานก่อสร้าง</a>
                            </li>
                            <li>
                                <a href="">ฝ่ายงานระบบ</a>
                            </li>
                            <li>
                                <a href="">ฝ่ายเทคโนโลยีน้ำ</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-12 col-md-3">
                    <div class="box-sitemap">
                        <h5 class="title">
                            โครงการของเรา
                        </h5>
                        <ul class="box-sitemap_list">
                            <li>
                                <a href="">สถานีไฟฟ้าแรงสูง</a>
                            </li>
                            <li>
                                <a href="">โรงไฟฟ้า</a>
                            </li>
                            <li>
                                <a href="">พลังงานทางเลือก</a>
                            </li>
                            <li>
                                <a href="">โรงงานปิโตรดคมี โรงกลั่น โรงแยกแก็ส</a>
                            </li>
                            <li>
                                <a href="">เหมืองแร่</a>
                            </li>
                            <li>
                                <a href="">งานก่อสร้างโรงงานและอาคาร</a>
                            </li>
                            <li>
                                <a href="">งานก่อสร้างระบบประกอบอาคารและโรงงาน</a>
                            </li>
                            <li>
                                <a href="">สถานีบริการน้ำมัน</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-12 col-md-3">
                    <div class="box-sitemap">
                        <h5 class="title">
                            ข่าวสาร
                        </h5>
                        <ul class="box-sitemap_list">
                            <li>
                                <a href="">ข่าวสาร</a>
                            </li>
                            <li>
                                <a href="">กิจกรรม</a>
                            </li>
                            <li>
                                <a href="">กิจกรรมเพื่อสังคม (CSR)</a>
                            </li>
                            <li>
                                <a href="">กิจกรรมการรับสมัครงาน</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-12 col-md-3">
                    <div class="box-sitemap">
                        <h5 class="title">
                            ร่วมงานกับเรา
                        </h5>
                        <ul class="box-sitemap_list">
                            <li>
                                <a href="">ร่วมงานกับเรา</a>
                            </li>

                        </ul>
                    </div>
                </div>
                <div class="col-12 col-md-3">
                    <div class="box-sitemap">
                        <h5 class="title">
                            ดาวน์โหลด
                        </h5>
                        <ul class="box-sitemap_list">
                            <li>
                                <a href="">วีดีทัศน์</a>
                            </li>
                            <li>
                                <a href="">ไฟล์</a>
                            </li>
                            <li>
                                <a href="">ltalthai 60th Year Projection Mapping</a>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>


     


<?php include 'include/footer.php';?>




