jQuery(document).ready(function($) {
    console.log('js');
    const $navMenu = $('nav#menu'),
            $body = $('body');
    if($navMenu.length > 0) {
        $navMenu.mmenu({
            "extensions": [
                "pagedim-black"
             ],
            "navbars": [
                {
                   "position": "bottom",
                   "content": [
                    '<a  href="contact.php"><i class="far fa-map-marker-alt"></i></a>',
                    '<a target=”_blank” href="#/"><i class="fab fa-facebook-f"></i></a>',
                    '<a target=”_blank” href="#/"><i class="fab fa-youtube"></i></a>',  
                   ]
                }
             ]
        });
    }
    $(window).resize(function(e){
        setPaddingTopWeb();
    });
    setPaddingTopWeb();
    function setPaddingTopWeb() {
        $body.css('paddingTop', 0 + 'px');
        const paddingTopWeb = $('.padding-HeaderWeb:visible').outerHeight();
        if(paddingTopWeb != "undefined") $body.css('paddingTop', paddingTopWeb + 'px');
        
    }


    // func check el in DOM
    // return jquerySelected or false if do not have
    function checkSelect(el) {
        return $(el).length > 0 ? $(el) : false;
    }

    const $gridProject = checkSelect('.grid-project-gallery');
    
    if($gridProject) {
        const $grid =  $gridProject.masonry({
                itemSelector: '.grid-project-gallery_item',
                gutter: 10,
                fitWidth: true
          });
        $grid.imagesLoaded().progress( function() {
            $grid.masonry('layout');
        });
    }

    const $fancy = checkSelect('[data-fancybox="gallery"]');
    if($fancy) {
        $fancy.fancybox({
            buttons: [
                "thumbs",
                "close"
              ],
        });
    }  


    // slider about
    // dots slider must be equal
    const $sliderAbout = checkSelect('#sliderAbout'),
          $dotAbouts = checkSelect('.dot-about >li'),
          $sliderAboutItem = checkSelect('.card-about');
    if($sliderAbout && $dotAbouts){
        if($sliderAboutItem.length ==  $dotAbouts.length) {
            // init slider item
            $sliderAbout.slick({
                arrows: false,
                draggable: false,
                fade: true
            });
            // event click dots to move slider
            $dotAbouts.click(function(e){
                e.stopPropagation();
                e.preventDefault();
                const $this = $(this);
                $dotAbouts.removeClass('active');
                $this.addClass('active');
                $('#sliderAbout').slick('slickGoTo',$this.index());
            });
        }
    }
  
    const $sliderHome = checkSelect('.slider-home');
    console.log($sliderHome);
    if($sliderHome) {
        $sliderHome.slick({
        dots: false,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 2000,
     });
    }

    const $bannerSlider = checkSelect('.banner-slider');
    if($bannerSlider) {
         $bannerSlider.slick({
        dots: true,
        arrows: false
     });
    }

    
    // set bg img home page
    const $boxSquare = checkSelect('.box-square');
    if($boxSquare) {
        $boxSquare.each(function(index,el){
            if($(el).data().bg == ''){
                console.warn($(el),'this div do not have data-bg');
            }
            $(el).css("background-image", 'url('+$(el).data().bg+')');
           
        });
    }
});