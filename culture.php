<?php include 'include/header.php';?>
        <div class="container">
            <nav class="d-flex align-items-center breadcrumb">
                <a href="">หน้าหลัก</a>
                <a class="active" href="">ดาวน์โหลด</a>
            </nav>
        </div>
        <div class="container">
            <section class="banner banner-about d-flex align-items-center">
                <h1>เกี่ยวกับเรา</h1>
            </section>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-12 col-md-3">
                    <h2 class="title-list">
                        เกี่ยวกับเรา
                    </h2>
                    <ul class="main-list">
                        <li>
                            <a href="">
                                ประวัติ
                            </a>
                        </li>
                        <li>
                            <a href="">
                                วิสัย
                            </a>
                        </li>
                        <li>
                            <a href="">
                                ปรัชญา
                            </a>
                        </li>
                        <li>
                            <a href="">
                                วัฒ
                            </a>
                        </li>
                        <li>
                            <a href="">
                                คณะ
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-12 col-md-9">

                    <div>
                        <h2 class="title-blue">
                            วัฒธรรมองค์กร
                        </h2>
                        <div class="container-fluid con-card-culture p-0">
                            <div class="row no-gutters row-card-culture">
                                <div class="col-12 col-md-6 ">
                                    <figure>
                                        <img src="https://via.placeholder.com/650x550" alt="">
                                    </figure>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="card-culture">
                                        <h5 class="title">
                                            หลักปฎิบัติเพื่อสร้างวัฒนธรรมที่เข้มแข็ง และ แข็งแกร่งของ บริษัท
                                            ิตัลไทยวิศวกรรม จำกัด
                                        </h5>
                                        <div>
                                            <h6 class="mini-title">
                                                title
                                            </h6>
                                            <p class="des">
                                                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Saepe
                                            </p>
                                        </div>
                                        <div>
                                            <h6 class="mini-title">
                                                title
                                            </h6>
                                            <p class="des">
                                                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Saepe
                                            </p>
                                        </div>
                                        <div>
                                            <h6 class="mini-title">
                                                title
                                            </h6>
                                            <p class="des">
                                                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Saepe
                                            </p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row no-gutters row-card-culture">
                                <div class="col-12 col-md-6">
                                    <figure>
                                        <img src="https://via.placeholder.com/650x550" alt="">
                                    </figure>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="card-culture">
                                        <div>
                                            <h6 class="mini-title">
                                                title
                                            </h6>
                                            <p class="des">
                                                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Saepe
                                            </p>
                                        </div>
                                        <div>
                                            <h6 class="mini-title">
                                                title
                                            </h6>
                                            <p class="des">
                                                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Saepe
                                            </p>
                                        </div>
                                        <div>
                                            <h6 class="mini-title">
                                                title
                                            </h6>
                                            <p class="des">
                                                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Saepe
                                            </p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

<?php include 'include/footer.php';?>
