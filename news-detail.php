
<?php include 'include/header.php';?>



        <div class="container">
            <nav class="d-flex align-items-center breadcrumb">
                <a href="">หน้าหลัก</a>
                <a class="active" href="">ดาวน์โหลด</a>
            </nav>
        </div>
        <div class="container">
            <section class="banner banner-news d-flex align-items-center">
                <h1>ข่าวสารและกิจกรรม</h1>
            </section>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-12 col-md-3">
                    <h2 class="title-list">
                        ข่าวสารอื่นๆ
                    </h2>
                    <ul class="main-list main-list-white">
                        <li>
                            <a href="">
                                อิตัลไทยวิศวกรรม เปิดเวที I-Leader Forum
                                พัฒนาผู้นำรุ่นใหม่
                            </a>
                        </li>
                        <li>
                            <a href="">
                                อิตัลไทยฯ รุกโรดแมพผู้นำรับเหมาอาเซียน
                            </a>
                        </li>
                        <li>
                            <a href="">
                                "อิตัลไทยวิศวกรรม" ฉลองครบรอบปีที่ 50 เดินหน้าพัฒนา อย่างมั่งคง
                            </a>
                        </li>
                        <li>
                            <a href="">
                                ITE ร่วมพิธี First Brick ก่ออิฐก้อนแร EGAT
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-12 col-md-9">
                    <div>
                        <h2 class="title-Lblue">
                            title Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        </h2>
                        <div class="d-flex justify-content-between align-items-center info">
                            <p class="date">
                                20/05/2561
                            </p>
                            <div class="d-flex social-share">
                                <a class="social-share_s" href=""><i class="far fa-share-alt"></i></a>
                                <a class="social-share_f" href=""><i class="fab fa-facebook-f"></i></a>
                                <a class="social-share_t" href=""><i class="fab fa-twitter"></i></a>
                                <a class="social-share_g" href=""><i class="fab fa-google-plus-g"></i></a>
                            </div>
                        </div>
                        <figure class="hero-img-detail">
                            <img src="https://via.placeholder.com/1150x450" alt="img banner detail">
                        </figure>
                        <p>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, facilis veritatis recusandae, rerum excepturi earum quod adipisci, esse cupiditate soluta alias officia magnam laboriosam. Consequatur perspiciatis odit pariatur magnam perferendis?
                        </p>
                        <p>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Distinctio aperiam earum omnis nemo enim tempore, unde dolorem, quasi ratione facere repellendus aut atque harum quod explicabo reiciendis repudiandae pariatur veniam!
                        </p>
                        <a class="btn-blue btn-m50" href="">
                            กลับ
                        </a>
                    </div>
                </div>
            </div>
        </div>



<?php include 'include/footer.php';?>