<?php include 'include/header.php';?>
        </header>

        <section class="container-fluid p-0">
            <div class="row">
                <div class="col-12">
                    <section class="wrap-slider-home">
                        <ul class="slider-home">
                            <li>
                                <a href="">
                                    <img class="d-block d-md-none img-hero-mobile" src="dist/img/banner-home-mobile.png" alt="img-hero">
                                    <img class="d-none d-md-block img-hero-destop" src="dist/img/sliderHome/banner-home-01.png" alt="img-hero">
                                </a>    
                            </li>
                            <li>
                                <a href="">
                                    <img class="d-block d-md-none img-hero-mobile" src="dist/img/banner-home-mobile.png" alt="img-hero">
                                    <img class="d-none d-md-block img-hero-destop" src="dist/img/sliderHome/banner-home-01.png" alt="img-hero">
                                </a>    
                            </li>
                            <li>
                                <a href="">
                                    <img class="d-block d-md-none img-hero-mobile" src="dist/img/banner-home-mobile.png" alt="img-hero">
                                    <img class="d-none d-md-block img-hero-destop" src="dist/img/sliderHome/banner-home-01.png" alt="img-hero">
                                </a>    
                            </li>
                        </ul>
                        <div class="d-none d-md-block img-slider-home img-slider-home-1">
                            <img src="dist/img/sliderHome/banner-home-02.png" alt="">
                        </div>
                        <div class="d-none d-md-block img-slider-home img-slider-home-2">
                            <img src="dist/img/sliderHome/banner-home-03.png" alt="">
                        </div>
                        <div class="d-block d-md-none img-slider-home img-slider-home-1">
                            <img src="dist/img/sliderHome/banner-home-mobile02.png" alt="">
                        </div>
                        <div class="d-block d-md-none img-slider-home img-slider-home-2">
                            <img src="dist/img/sliderHome/banner-home-mobile03.png" alt="">
                        </div>
                    </section>
                </div>
            </div>
        </section>

        <section class="container-fluid">
            <div class="row justify-content-md-center bg-title-welcome">
                <div class="col-12">
                    <div class="text-center title-welcome">
                        <h1 class="title">
                            อิตัลไทยวิศวกรรม <br class="d-block d-md-none">
                            บริษัทรับเหมางานวิศกรรมชั้นนำของไทย
                        </h1>
                        <p class="des">
                            ดำเนินะรุกิจในการเป็นผู้เชี่ยวชาญงานวิศวกรรมก่อสร้างแบบ EPC หรือ Turnkey ในทุกสาขา
                            ครอบคลุมงานออกแบบวิศกรรม งานจัดซื้อ จัดหางานก่อสร้าง ติดตั้งและทดสอบนำเข้าใข้งาน<br class="d-none d-xl-block">
                            และงานบำรุงรักษา ภายใต้การรับรองมาตราฐานระบบบริหารคุณภาพ ISO 9001:2015,
                            และระบบบริหารอาชีวอนามัยและความปลอดภัยตามมาตราฐาน OHSA18001:2007<br class="d-none d-xl-block">
                            และระบบบริหารสิ่งแวดล้อมมาตราฐาน ISO14001:2015
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <section class="container">
            <div class="row no-gutters wrap-card-partner">
                <div class="col-12 col-md-4">
                    <a href="google.com" class="card-partner">
                        <figure class="card-partner_img">
                            <img src="dist/img/elec.png" alt="">
                        </figure>
                        <div data-theme="blue" class="card-partner_body">
                            <h2>
                                Smart Grid Group
                            </h2>
                            <p>
                                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dignissimos, animi deserunt.
                                Accusamus voluptatum .
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-12 col-md-4">
                    <a href="google.com" class="card-partner">
                        <figure class="card-partner_img">
                            <img src="dist/img/sora.png" alt="">
                        </figure>
                        <div data-theme="white" class="card-partner_body">
                            <h2>
                                Smart Grid Group
                            </h2>
                            <p>
                                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dignissimos, animi deserunt.
                                Accusamus voluptatum .
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-12 col-md-4">
                    <a href="google.com" class="card-partner">
                        <figure class="card-partner_img">
                            <img src="dist/img/bus.png" alt="">
                        </figure>
                        <div data-theme="black" class="card-partner_body">
                            <h2>
                                Smart Grid Group
                            </h2>
                            <p>
                                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dignissimos, animi deserunt.
                                Accusamus voluptatum .
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-12 col-md-4">
                    <a href="google.com" class="card-partner">
                        <figure class="card-partner_img">
                            <img src="dist/img/town.png" alt="">
                        </figure>
                        <div data-theme="blue" class="card-partner_body">
                            <h2>
                                Smart Grid Group
                            </h2>
                            <p>
                                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dignissimos, animi deserunt.
                                Accusamus voluptatum .
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-12 col-md-4">
                    <a href="google.com" class="card-partner">
                        <figure class="card-partner_img">
                            <img src="dist/img/figure.png" alt="">
                        </figure>
                        <div data-theme="green" class="card-partner_body">
                            <h2>
                                Smart Grid Group
                            </h2>
                            <p>
                                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dignissimos, animi deserunt.
                                Accusamus voluptatum .
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-12 col-md-4">
                    <a href="google.com" class="card-partner">
                        <figure class="card-partner_img">
                            <img src="dist/img/reward.png" alt="">
                        </figure>
                        <div data-theme="blue" class="card-partner_body">
                            <h2>
                                Smart Grid Group
                            </h2>
                            <p>
                                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dignissimos, animi deserunt.
                                Accusamus voluptatum .
                            </p>
                        </div>
                    </a>
                </div>
            </div>
        </section>

        <section class="container">
            <div class="row no-gutters wrap-box-square">
                <div class="col-12 col-md-12 col-lg-6">
                    <div class="d-flex flex-wrap">
                        <a data-theme="green" data-bg="dist/img/docu.png" class="box-square" href="">
                            <h4 class="title">Procurement</h4>
                        </a>
                        <a data-theme="brown" data-bg="dist/img/women.png" class="box-square" href="">
                            <h4 class="title">Human Resource</h4>
                        </a>
                    </div>
                    <div class="d-flex flex-wrap">
                        <a data-theme="dark-brown" data-bg="dist/img/laptop.png" class="box-square" href="">
                            <h4 class="title">ITE News</h4>
                        </a>
                        <a data-theme="blue" data-bg="dist/img/pen.png" class="box-square" href="">
                            <h4 class="title">Business Development</h4>
                        </a>
                    </div>

                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <a data-theme="green" data-bg="dist/img/persons.png" class="box-square h100" href="">
                        <h4 class="title">Board of Direcor & <br> Board of Management</h4>
                    </a>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <a data-theme="transparent" data-bg="dist/img/image_1.png" class="box-square h100" href="">
                    </a>
                </div>
            </div>
        </section>

<?php include 'include/footer.php';?>

     