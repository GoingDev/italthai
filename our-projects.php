
<?php include 'include/header.php';?>



        <div class="container">
            <nav class="d-flex align-items-center breadcrumb">
                <a href="">หน้าหลัก</a>
                <a class="active" href="">โครงการของเรา</a>
            </nav>
        </div>
        <div class="container">
            <section class="banner banner-ourproject d-flex align-items-center">
                <h1>โครงการของเรา</h1>
            </section>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-12 col-md-3">
                    <h2 class="title-list">
                        โครงการของเรา
                    </h2>
                    <ul class="main-list">
                        <li>
                            <a href="">สถานีไฟฟ้าแรงสูง</a>
                        </li>
                        <li>
                            <a href="">โรงไฟฟ้า</a>
                        </li>
                        <li>
                            <a href="">พลังงานทางเลือก</a>
                        </li>
                        <li>
                            <a href="">โรงงานปิโตรเคมี โรงกลั่น</a>
                        </li>
                        <li>
                            <a href="">เหมืองแร่</a>
                        </li>
                        <li>
                            <a href="">งานก่อสร้างโรงงานและธนาคาร</a>
                        </li>
                        <li>
                            <a href="">สถานีบริการน้ำมัน</a>
                        </li>
                    </ul>
                </div>
                <div class="col-12 col-md-9">
                    <h2 class="title-blue">
                        สถานีไฟฟ้าแรงสูง
                    </h2>
                    <a class='project' href="">
                        <figure class="image">
                            <img src="" alt="">
                        </figure>
                        <div class="project_body">
                            <h5 class="title">
                                โครงการสถานีไฟฟ้าปิ่นทองและพานทอง 2
                            </h5>
                            <p class="des">
                                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Voluptatibus delectus quo
                                quam velit voluptas, praesentium iusto reprehenderit iure! Quidem, at facere neque
                                nesciunt accusamus dicta corporis fugiat maiores non iste.
                            </p>
                            <h6 class="mini-title">
                                ผู้ว่าจ้าง : การไฟฟ้านครหลวง
                            </h6>
                            <p class="btn">
                                รายละเอียด
                            </p>
                        </div>
                    </a>
                    <a class='project' href="">
                        <figure class="image">
                            <img src="" alt="">
                        </figure>
                        <div class="project_body">
                            <h5 class="title">
                                โครงการสถานีไฟฟ้าปิ่นทองและพานทอง 2
                            </h5>
                            <p class="des">
                                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Voluptatibus delectus quo
                                quam velit voluptas, praesentium iusto reprehenderit iure! Quidem, at facere neque
                                nesciunt accusamus dicta corporis fugiat maiores non iste.
                            </p>
                            <h6 class="mini-title">
                                ผู้ว่าจ้าง : การไฟฟ้านครหลวง
                            </h6>
                            <p class="btn">
                                รายละเอียด
                            </p>
                        </div>
                    </a>
                    <a class='project' href="">
                        <figure class="image">
                            <img src="" alt="">
                        </figure>
                        <div class="project_body">
                            <h5 class="title">
                                โครงการสถานีไฟฟ้าปิ่นทองและพานทอง 2
                            </h5>
                            <p class="des">
                                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Voluptatibus delectus quo
                                quam velit voluptas, praesentium iusto reprehenderit iure! Quidem, at facere neque
                                nesciunt accusamus dicta corporis fugiat maiores non iste.
                            </p>
                            <h6 class="mini-title">
                                ผู้ว่าจ้าง : การไฟฟ้านครหลวง
                            </h6>
                            <p class="btn">
                                รายละเอียด
                            </p>
                        </div>
                    </a>
                    <a class='project' href="">
                        <figure class="image">
                            <img src="" alt="">
                        </figure>
                        <div class="project_body">
                            <h5 class="title">
                                โครงการสถานีไฟฟ้าปิ่นทองและพานทอง 2
                            </h5>
                            <p class="des">
                                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Voluptatibus delectus quo
                                quam velit voluptas, praesentium iusto reprehenderit iure! Quidem, at facere neque
                                nesciunt accusamus dicta corporis fugiat maiores non iste.
                            </p>
                            <h6 class="mini-title">
                                ผู้ว่าจ้าง : การไฟฟ้านครหลวง
                            </h6>
                            <p class="btn">
                                รายละเอียด
                            </p>
                        </div>
                    </a>
                    <div class="d-flex justify-content-end">
                        <nav class="pagination">
                            <a href=""><i class="fal fa-angle-double-left"></i></a>
                            <a class="active" href="">1</a>
                            <a href="">2</a>
                            <a href="">3</a>
                            <a href=""><i class="fal fa-angle-double-right"></i></a>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

<?php include 'include/footer.php';?>


      