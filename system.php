<?php include 'include/header.php';?>

        <div class="container">
            <nav class="d-flex align-items-center breadcrumb">
                <a href="">หน้าหลัก</a>
                <a class="active" href="">ระบบบริหาร</a>
            </nav>
        </div>
        <div class="container">
            <section class="banner banner-system d-flex align-items-center">
                <h1>ระบบบริหาร</h1>
            </section>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-12 col-md-3">
                    <h2 class="title-list">
                        ระบบบริหาร
                    </h2>
                    <ul class="main-list">
                        <li>
                            <a href="">
                                ประวัติ
                            </a>
                        </li>
                        <li>
                            <a href="">
                                วิสัย
                            </a>
                        </li>
                        <li>
                            <a href="">
                                ปรัชญา
                            </a>
                        </li>
                        <li>
                            <a href="">
                                วัฒ
                            </a>
                        </li>
                        <li>
                            <a href="">
                                คณะ
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-12 col-md-9">

                    <div class="container-fluid p-0 bg-system">
                        <div class="row no-gutters row-system">
                            <div class="col-12 col-lg-5 order-lg-2">
                                <figure>
                                    <img class="human-managenment" src="dist/img/human-managenment-system.png" alt="man board">
                                </figure>
                            </div>
                            <div class="col-12 col-lg-7">
                                <h2 class="title-blue title-systeam">
                                    สาสน์จากประธานเจ้าหน้าที่บริหาร
                                </h2>
                                <p class="des">
                                <strong>
                                    ความมุ่งมั่นของฝ่ายบริหารและนโยบายในการให้บริการ
                                </strong>
                                </p>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Vero eius velit voluptatem
                                    molestiae perspiciatis vitae quaerat aperiam. Quaerat, inventore, voluptas repellat
                                    amet
                                    corrupti quis nisi praesentium esse expedita quos cumque.</p>
                                <ol class="order-list">
                                    <li>
                                        Lorem ipsum dolor sit, amet consectetur adipisicing elit. Explicabo nihil similique maxime mollitia impedit soluta culpa dolorem a est obcaecati accusantium, eligendi error recusandae maiores delectus dolores repellendus vero inventore.
                                    </li>
                                    <li>
                                        รูปแบบ
                                    </li>
                                    <li>
                                        ระบบบริหารอาชีว
                                    </li>
                                    <li>
                                        ระบบบริหารสิ่งแวดล้อม
                                    </li>
                                    <li>
                                        การได้รับอนุญาติ
                                    </li>
                                </ol>
                                <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Neque nemo aperiam sit
                                    tempora?
                                    Veniam, rerum optio iusto possimus sequi, qui nesciunt temporibus aut voluptatem,
                                    esse enim
                                    praesentium deserunt. Aliquam, ipsam.</p>
                               
                            </div>
                        </div>
                        <h6 class="title-blue title-t">
                                บริษัทได้กำหนดนโยบายในการบริหารการให้บริการดังนี้
                            </h6>
                        <div class="row row-text-system">
                                <div class="col-12 col-md-4">
                                        <div class="text-system">
                                            <h5 class="title">
                                                นโยบายคุณภาพ
                                            </h5>
                                            <p class="des">
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni,
                                                iusto
                                                sapiente eaque qui ratione totam eius et animi asperiores fugit
                                                quod
                                                voluptates deleniti nisi voluptate! Cum saepe excepturi commodi
                                                repellendus!
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="text-system">
                                            <h5 class="title">
                                                นโยบายอาชีวอนามัยและความปลอดภัย
                                            </h5>
                                            <p class="des">
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni,
                                                iusto
                                                sapiente eaque qui ratione totam eius et animi asperiores fugit
                                                quod
                                                voluptates deleniti nisi voluptate! Cum saepe excepturi commodi
                                                repellendus!
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="text-system">
                                            <h5 class="title">
                                                นโยบายสิ่งแวดล้อม
                                            </h5>
                                            <p class="des">
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni,
                                                iusto
                                                sapiente eaque qui ratione totam eius et animi asperiores fugit
                                                quod
                                                voluptates deleniti nisi voluptate! Cum saepe excepturi commodi
                                                repellendus!
                                            </p>
                                        </div>
                                    </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

<?php include 'include/footer.php';?>

