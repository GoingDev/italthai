<?php include 'include/header.php';?>



        <div class="container">
            <nav class="d-flex align-items-center breadcrumb">
                <a href="">หน้าหลัก</a>
                <a class="active" href="">ดาวน์โหลด</a>
            </nav>
        </div>
        <div class="container">
            <section class="banner banner-about d-flex align-items-center">
                <h1>เกี่ยวกับเรา</h1>
            </section>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-12 col-md-3">
                    <h2 class="title-list">
                        เกี่ยวกับเรา
                    </h2>
                    <ul class="main-list">
                        <li>
                            <a href="">
                                ประวัติ
                            </a>
                        </li>
                        <li>
                            <a href="">
                                วิสัย
                            </a>
                        </li>
                        <li>
                            <a href="">
                                ปรัชญา
                            </a>
                        </li>
                        <li>
                            <a href="">
                                วัฒ
                            </a>
                        </li>
                        <li>
                            <a href="">
                                คณะ
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-12 col-md-9">
                    <div>
                        <h2 class="title-blue">
                            ประวัติความเป็นมา
                        </h2>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas asperiores ipsa molestiae
                            culpa illum in eligendi numquam, dolore cupiditate sunt recusandae esse tenetur
                            necessitatibus libero, amet ad illo! Vero, quis.</p>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam id veritatis vitae modi
                            eius harum enim exercitationem necessitatibus! Suscipit delectus reprehenderit dolorem
                            recusandae facilis autem nobis aut repellendus, iure aliquam?</p>
                    </div>
                    <div class="scroll-x scrollx-thin">
                        <div class="wrap-dot-about">
                            <ul class="dot-about">
                                <li class="active">
                                    <div class="dot-about_cycle"></div>
                                    <p>พ.ศ.2538-2545</p>
                                </li>
                                <li>
                                    <div class="dot-about_cycle"></div>
                                    <p>พ.ศ.2544-2546</p>
                                </li>
                                <li>
                                    <div class="dot-about_cycle"></div>
                                    <p>พ.ศ.2546-2550</p>

                                </li>
                                <li>
                                    <div class="dot-about_cycle"></div>
                                    <p>พ.ศ.2551-2556</p>
                                </li>
                                <li>
                                    <div class="dot-about_cycle"></div>
                                    <p>พ.ศ.2557 จนถึงปัจจุบัน</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <ul id="sliderAbout" class="slider-about">
                        <li>
                            <div class="card-about">
                                <h5 class="title">
                                    title0
                                </h5>
                                <h6 class="mini-title">
                                    mini-title
                                </h6>
                                <p class="des">
                                    Lorem ipsum dolor sit amet consectetur, adipisicing elit. Praesentium
                                    deleniti laborum quasi illum dolores, distinctio obcaecati error rem.
                                    Ipsum
                                    nisi molestiae quod facere nemo aliquam itaque harum quisquam animi
                                    libero.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="card-about">
                                <h5 class="title">
                                    title1
                                </h5>
                                <h6 class="mini-title">
                                    mini-title
                                </h6>
                                <p class="des">
                                    Lorem ipsum dolor sit amet consectetur, adipisicing elit. Praesentium
                                    deleniti laborum quasi illum dolores, distinctio obcaecati error rem.
                                    Ipsum
                                    nisi molestiae quod facere nemo aliquam itaque harum quisquam animi
                                    libero.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="card-about">
                                <h5 class="title">
                                    title2
                                </h5>
                                <h6 class="mini-title">
                                    mini-title
                                </h6>
                                <p class="des">
                                    Lorem ipsum dolor sit amet consectetur, adipisicing elit. Praesentium
                                    deleniti laborum quasi illum dolores, distinctio obcaecati error rem.
                                    Ipsum
                                    nisi molestiae quod facere nemo aliquam itaque harum quisquam animi
                                    libero.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="card-about">
                                <h5 class="title">
                                    title3
                                </h5>
                                <h6 class="mini-title">
                                    mini-title
                                </h6>
                                <p class="des">
                                    Lorem ipsum dolor sit amet consectetur, adipisicing elit. Praesentium
                                    deleniti laborum quasi illum dolores, distinctio obcaecati error rem.
                                    Ipsum
                                    nisi molestiae quod facere nemo aliquam itaque harum quisquam animi
                                    libero.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="card-about">
                                <h5 class="title">
                                    title4
                                </h5>
                                <h6 class="mini-title">
                                    mini-title
                                </h6>
                                <p class="des">
                                    Lorem ipsum dolor sit amet consectetur, adipisicing elit. Praesentium
                                    deleniti laborum quasi illum dolores, distinctio obcaecati error rem.
                                    Ipsum
                                    nisi molestiae quod facere nemo aliquam itaque harum quisquam animi
                                    libero.
                                </p>
                            </div>
                        </li>
                    </ul>



                </div>

            </div>
        </div>

<?php include 'include/footer.php';?>






















