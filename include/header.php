<!DOCTYPE html>
<html lang="en">

<head>
    <title>Italthai</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">


    <!-- CSS -->
    <link href="https://fonts.googleapis.com/css?family=Prompt:300,400,600,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />

    <link rel="stylesheet" href="dist/css/jquery.mmenu.css">
    <link rel="stylesheet" href="dist/css/jquery.mmenu.navbars.css">
    <link rel="stylesheet" href="dist/css/jquery.mmenu.pagedim.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.2/jquery.fancybox.min.css">
    <link rel="stylesheet" href="dist/css/style.min.css">
</head>
<!-- Start div id 'page' -->
<div id="page">
<!-- Start div class 'content' -->
    <div class="content">

<!-- Start Header Mobile and Destop -->
        <header id="headerWeb" class="d-none d-lg-block padding-HeaderWeb">
            <nav class="top-nav">
                <div class="container">
                    <ul class="d-flex justify-content-end  top-nav_menu">
                        <li>
                            <a href="contact.php">
                                ติดต่อเรา
                            </a>
                        </li>
                        <li>
                            <a href="sitemap.php">แผนผังเว็บไซต์</a>
                        </li>
                        <li>
                            <a href="">เฉพาะพนักงาน</a>
                            <i class="fal fa-angle-down"></i>
                            <ul class="top-nav_sub">
                                <li>
                                    <a href="">email</a>
                                </li>
                                <li>
                                    <a href="">
                                        เว็บนอก
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="">ไทย</a>
                            <i class="fal fa-angle-down"></i>
                            <ul class="top-nav_sub">
                                <li>
                                    <a href="">Eng</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
            <nav class="main-nav">
                <div class="container">
                    <div class="d-flex justify-content-between align-items-center">
                        <a href="index.php">
                            <img class="main-nav_logo" src="dist/img/logo-right.png" alt="logo">
                        </a>
                        <ul class="d-flex main-nav_menu">
                            <li class='active'><a href="index.php">หน้าหลัก</a></li>
                            <li><a href="about.php">เกี่ยวกับเรา</a></li>
                            <li><a href="system.php">ระบบบริหาร</a></li>
                            <li><a href="our-business.php">ธรุกิจของเรา</a></li>
                            <li><a href="our-projects.php">โครงการของเรา</a></li>
                            <li><a href="news.php">ข่าวสาร</a></li>
                            <li><a href="job-us.php">ร่วมงานกับเรา</a></li>
                            <li><a href="download.php">ดาวน์โหลด</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        <div class="header d-block d-lg-none">
            <div class="container">
                <div class="d-flex align-items-center header-box">
                    <a href="#menu"><i class="far fa-bars"></i></a>
                    <figure class="mx-auto">
                        <img class="main-nav_logo--mobile" src="dist/img/logo-right.png" alt="mobile-logo">
                    </figure>
                </div>
            </div>
        </div>
<!-- End Header Mobile and Destop -->