<!-- Start Footer -->
<footer id="footerWeb" class="main-footer">
            <figure class="d-inline-block">
                <img class="logo-footer" src="dist/img/logo-bottom.png" alt="logo_footer">
            </figure>
            <div>
                <div>
                    <a target=”_blank” class="footer_address link-h-underline" href="https://goo.gl/maps/3TMhNYRCxmR2">
                        2034/124 อาคารอิตัลไทยทาวเวอร์ ชั้น 29 ถ.เพชรบุรีตัดใหม่ แขวงบางกะปิ เขตห้วยขวาง กทม. 10310
                    </a>
                </div>
                <div class="d-flex justify-content-center footer_contact">
                    <a href="tel:0-2723-4420-5" class="link-h-underline">
                        โทร. 0-2723-4420-5
                    </a>
                    <span class="line">
                        |
                    </span>
                    <a href="tel:0-2723-4427" class="link-h-underline">
                        โทรสาร. 0-2723-4427
                    </a>
                </div>
                <div class="d-flex justify-content-center footer_social">
                    <a target=”_blank” href="https://www.facebook.com/">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                    <a target=”_blank” href="https://www.youtube.com/">
                        <i class="fab fa-youtube"></i>
                    </a>
                </div>
            </div>
        </footer>
<!-- End Footer -->
    </div> 
<!-- End div class 'content' -->


<!-- Start main Menu Mobile -->
    <nav id="menu">
        <div id="panel-menu">
            <ul>
                <li><a href="index.php">หน้าหลัก</a></li>
                <li><a href="about.php">เกี่ยวกับเรา</a></li>
                <li><a href="system.php">ระบบบริหาร</a></li>
                <li><a href="our-business.php">ธรุกิจของเรา</a></li>
                <li><a href="our-projects.php">โครงการของเรา</a></li>
                <li><a href="news.php">ข่าวสาร</a></li>
                <li><a href="job-us.php">ร่วมงานกับเรา</a></li>
                <li><a href="download.php">ดาวน์โหลด</a></li>
            </ul>
        </div>
    </nav>
<!-- End main Menu Mobile -->


</div> 
<!-- End div id 'page' -->



<!-- Start include Script Javascript  -->

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="dist/js/jquery.mmenu.js">
    </script>
    <script src="dist/js/jquery.mmenu.navbars.js">
    </script>
    <script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.2/dist/jquery.fancybox.min.js"></script>
    <script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>


    <script src="dist/js/script.min.js"></script>
<!-- End include Script Javascript  -->
</body>

</html>