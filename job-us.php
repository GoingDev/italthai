
<?php include 'include/header.php';?>


        <div class="container">
            <nav class="d-flex align-items-center breadcrumb">
                <a href="">หน้าหลัก</a>
                <a class="active" href="">ดาวน์โหลด</a>
            </nav>
        </div>
        <div class="container">
            <section class="banner banner-jobUs d-flex align-items-center">
                <h1>ร่วมงานกับเรา</h1>
            </section>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-12 col-md-3">
                    <h2 class="title-list">
                        รายละเอียดงาน
                    </h2>
                    <ul class="main-list main-list-jobUs">
                        <li>
                            <a href="">
                                <p>เงินเดือน : </p>
                                <p>ตามโครงสร้างบริษัท</p>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <p>สถานที่ทำงาน : </p>
                                <p>สำนักงานใหญ่</p>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <p>ประเภทงาน : </p>
                                <p>พนักงานประจำ</p>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-12 col-md-9">
                    <div>
                        <div class="wrap-title-job-us">
                            <div class="d-flex justify-content-between">
                                <h2 class="title-blue">วิศวกรประเมินราคา</h2>
                                <a class="btn-blue" href="">ย้อนกลับ</a>
                            </div>
                            <h3 class="mini-title-Lblue">
                                วิศกรโยธา
                            </h3>
                        </div>
                        <form action="" class="form-job-us">
                            <div class="wrap-input">
                                <input type="text" placeholder="ชื่อ-นามสกุล">
                                <input type="tel" name="" id="" placeholder="เบอร์โทรศัพท์">
                            </div>
                            <div class="wrap-input">
                                <input type="email" name="" id="" placeholder="อีเมล์">
                            </div>
                            <div class="wrap-input">
                                <textarea name="" id="" placeholder="รายละเอียด"></textarea>
                            </div>
                            <div class="wrap-input">
                                <label for="">Resume</label>
                                <input type="file" name="" id="">
                            </div>
                            <button type="submit">ส่งข้อมูล</button>
                        </form>

                        <div class="wrap-text-jobUs">
                            <p class="c-blue">รายละเอียดงาน</p>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Odit, nisi? Modi omnis
                                laboriosam excepturi perspiciatis. Sint voluptatem, sit quidem, dolor inventore cumque
                                quasi quibusdam deleniti odit rerum rem quo earum?
                            </p>
                        </div>
                        <div class="wrap-text-jobUs">
                            <p class="c-blue">รายละเอียดงาน</p>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Odit, nisi? Modi omnis
                                laboriosam excepturi perspiciatis. Sint voluptatem, sit quidem, dolor inventore cumque
                                quasi quibusdam deleniti odit rerum rem quo earum?
                            </p>
                        </div>
                        <div class="wrap-text-jobUs">
                            <p class="c-blue">รายละเอียดงาน</p>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Odit, nisi? Modi omnis
                                laboriosam excepturi perspiciatis. Sint voluptatem, sit quidem, dolor inventore cumque
                                quasi quibusdam deleniti odit rerum rem quo earum?
                            </p>
                        </div>
                    </div>
                    <div class="jobUs-map-g">
                        <p class="c-blue title">สถานที่ตั้ง</p>
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3875.535048929503!2d100.57155201532517!3d13.746576090350878!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e29ef7cc94d4d7%3A0x92216cebd91706f7!2sItalthai+Tower+(ITD)!5e0!3m2!1sen!2sth!4v1542698933789" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                        <a class="btn-blue btn-dBlue d-flex" target="_blank" href="https://goo.gl/maps/q7ULjMM7WQv">ขอเส้นทาง</a>
                    </div>
                </div>
            </div>
        </div>


<?php include 'include/footer.php';?>