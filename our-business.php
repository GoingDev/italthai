<?php include 'include/header.php';?>

        <div class="container">
            <nav class="d-flex align-items-center breadcrumb">
                <a href="">หน้าหลัก</a>
                <a class="active" href="">ดาวน์โหลด</a>
            </nav>
        </div>
        <div class="container">
            <section class="wrap-banner-slider">
                <ul class="banner-slider">
                    <li>
                        <div class="banner-slider_item">
                            <figure>
                                <img src="https://via.placeholder.com/780x400" alt="">
                            </figure>
                            <div class="d-flex justify-content-center align-items-center banner-slider_item-bg">
                                <h3 class="title d-inline-block">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio dolore dolores excepturi aliquam architecto, labore temporibus reiciendis

                                </h3>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="banner-slider_item">
                            <figure>
                                <img src="https://via.placeholder.com/780x400" alt="">
                            </figure>
                            <div class="d-flex justify-content-center align-items-center banner-slider_item-bg">
                                <h3 class="title d-inline-block">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio dolore dolores excepturi aliquam architecto, labore temporibus reiciendis
                                </h3>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="banner-slider_item">
                            <figure>
                                <img src="https://via.placeholder.com/780x400" alt="">
                            </figure>
                            <div class="d-flex justify-content-center align-items-center banner-slider_item-bg">
                                <h3 class="title d-inline-block">
                                    title
                                </h3>
                            </div>
                        </div>
                    </li>
                </ul>
            </section>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-12 col-md-3">
                    <h2 class="title-list">
                        ระบบบริหาร
                    </h2>
                    <ul class="main-list">
                        <li>
                            <a href="">
                                ประวัติ
                            </a>
                        </li>
                        <li>
                            <a href="">
                                วิสัย
                            </a>
                        </li>
                        <li>
                            <a href="">
                                ปรัชญา
                            </a>
                        </li>
                        <li>
                            <a href="">
                                วัฒ
                            </a>
                        </li>
                        <li>
                            <a href="">
                                คณะ
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-12 col-md-9">
                    <div>
                        <h2 class="title-blue">
                            ฝ่ายผู้เชียวชาญระบบไฟฟ้า
                        </h2>
                        <div class="container-fluid p-0">
                            <div class="row">
                                <div class="col-12 col-md-4">
                                    <div class="box-business">
                                        <h5 class="title">
                                            title
                                        </h5>
                                        <ul class="business-list">
                                            <li>งานสถานี งานสถานี งานสถานี งานสถานี</li>
                                            <li>งานสถานี</li>
                                            <li>งานสถานี</li>
                                            <li>งานสถานี</li>
                                            <li>งานสถานี</li>
                                            <li>งานสถานี</li>
                                            <li>งานสถานี</li>
                                            <li>งานสถานี</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-12 col-md-4">
                                    <div class="box-business">
                                        <h5 class="title">
                                            title
                                        </h5>
                                        <ul class="business-list">
                                            <li>งานสถานี</li>
                                            <li>งานสถานี</li>
                                            <li>งานสถานี</li>
                                            <li>งานสถานี</li>
                                            <li>งานสถานี</li>
                                            <li>งานสถานี</li>
                                            <li>งานสถานี</li>
                                            <li>งานสถานี</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-12 col-md-4">
                                    <div class="box-business">
                                        <h5 class="title">
                                            title
                                        </h5>
                                        <ul class="business-list">
                                            <li>งานสถานี</li>
                                            <li>งานสถานี</li>
                                            <li>งานสถานี</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="business-images">
                            <div class="d-flex">
                                <figure>
                                    <img src="https://via.placeholder.com/450" alt="">
                                </figure>
                                <figure>
                                    <img src="https://via.placeholder.com/450" alt="">
                                </figure>
                            </div>
                            <div class="d-flex">
                                <figure>
                                    <img src="https://via.placeholder.com/900x450" alt="">
                                </figure>
                            </div>
                        </div>
                        
                        <div class="business-contact">
                            <h6 class="title">
                                ผู้ติดต่อ
                            </h6>
                            <h5 class="des">
                                งานไฟฟ้า และงานบำรุงรักษา
                            </h5>
                            <a href="">
                                <p>โทร :</p>
                                  +66(0) 2723-4420-5 ต่อ4001
                            </a>
                            <a href="">
                                <p>มือถือ :</p>
                                  +66(8) 1914-2233
                            </a>
                            <a href="">
                                <p>อีเมล์ :</p>
                                  tcv@italthaiengineering.com
                            </a>
                        </div>
                    </div>
                </div>

            </div>
        </div>

<?php include 'include/footer.php';?>
